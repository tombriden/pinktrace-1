#!/usr/bin/env python
# coding: utf-8
# Copyright 2010, 2013 Ali Polatel <alip@exherbo.org>
# Distributed under the same terms as Python itself.

"""\
pinktrace Python bindings (__main__ module declaration)
"""

from pinktrace.test import *
main()
